﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using cs481_homework6.Models;
using System.Diagnostics;
using Xamarin.Essentials;

/*
This file will implement the owlbot definition api, it will also check for internet connectivity using xamarin.essentials package. 

*/
namespace cs481_homework6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()

        {
            InitializeComponent();

        }
        //function called when the search button is clicked on the app.
        private async void B1_Clicked(object sender, EventArgs e)
        {
            if(Connectivity.NetworkAccess != NetworkAccess.Internet)
            {//checks if there is no network connection.
                //if there is no network connection change the label to an error
                //and clear all the other labels then return.
                errorWord.Text = "Error. Can't connect to the internet";
                currentword.Text = "";
                L2.Text = "";
                L3.Text = "";
                L4.Text = "";
                return;
            }
            var EntryText = E1.Text; //make a variable entryText that will be the value of the user entry

            if (checkIfNull(EntryText))
            {//checks if the entry is null
                //if it is display error and return.
                errorWord.Text = "Please enter a word.";
                currentword.Text = "";
                L2.Text = "";
                L3.Text = "";
                L4.Text = "";
                return;
            }
            if(EntryText[0] == ' ')
            {//checks if the first character is a space
                //if it is trim off all the leading spaces
                EntryText = EntryText.Trim();
                if (checkIfNull(EntryText))
                {//then check if the trimmed word is null
                    //if it is display error and return
                    errorWord.Text = "Please enter a word.";
                    currentword.Text = "";
                    L2.Text = "";
                    L3.Text = "";
                    L4.Text = "";
                    return;
                }
                //if it is not null continue.
            }
            string link = "https://owlbot.info/api/v2/dictionary/" + EntryText; //link that will be the call with the user entered word.
            HttpClient client = new HttpClient();//making a new http client
            var uri = new Uri(link);//uri with the link entered.
            var request = new HttpRequestMessage();//var request has the http request message
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            HttpResponseMessage response = await client.SendAsync(request);//get the response from the request
            Definition[] words = null;//create an array of type definitions from the JSON file
            string code = response.StatusCode.ToString();//change the response code to a string and assign it to variable code
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {//if the response code is a success 
                var content = await response.Content.ReadAsStringAsync();//assign content to the content of the JSON
                words = Definition.FromJson(content);//store the converted JSON content into words array
                errorWord.Text = "";
                currentword.Text = "Word: " + EntryText; //display the word
                L2.Text = "Definition: " + words[0].DefinitionDefinition;//display the definition
                L3.Text = "Type: " + words[0].Type;//display the type
                L4.Text = "Example: " + words[0].Example;//display an example.

            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {//if the status code is not found
                //display error below
                errorWord.Text = "Error. No corresponding word.";
                currentword.Text = "";
                L2.Text = "";
                L3.Text = "";
                L4.Text = "";
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {//if the status code is a bad request
                //display error below.
                errorWord.Text = "Bad request";
                currentword.Text = "";
                L2.Text = "";
                L3.Text = "";
                L4.Text = "";
            }
        }

        //This function is called when the page appears.
        //It checks for internet connectivity
        //uses xamarin.essentials package
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            //if the connectivity is changed then call the connectivity changed function
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
        }

        //this function is called if the connection is changed and display an error message.
        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if(e.NetworkAccess != NetworkAccess.Internet)
            {//if there is no network access
                //display the alert with the error.
                DisplayAlert("Error", "You are not connected to the internet. Please reconnect to use this app.", "Ok");
            }
        }

        //checkIfNull returns true or false if the string given is empty or not
        private bool checkIfNull(string word)
        {
            if(String.IsNullOrEmpty(word))
            {//if it is empty return true
                return true;
            }
            else
            {//if it is not empty return false.
                return false;
            }
        }
    }
}
