﻿namespace cs481_homework6.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    /*
     This file holds all the JSON content from the OWL bot API stored into c# objects.
        Uses Netonsoft Json Package to convert back and forth.
     */
    public partial class Welcome
    {//class welcome holds an array of definitions 
        [JsonProperty("definitions")]
        public Definition[] Definitions { get; set; }
        //array of definitions for all the definitions of a specific word
        [JsonProperty("word")]
        public string Word { get; set; }
        //the word that the definition belongs to.
        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
        //the pronunciation of the word
    }

    public partial class Definition
    {//class definition has all the specific attributes related to the word

        [JsonProperty("type")]
        public string Type { get; set; }
        //type has the type of the word
        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }
        //definitondefinition is the definition of the word
        [JsonProperty("example")]
        public string Example { get; set; }
        //example is an example of the word
        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }
        //imageUrl holds the url of an image if it exists for the word
        [JsonProperty("emoji")]
        public object Emoji { get; set; }
        //emoji holds an emoji for the word if it exists
    }

 
    public partial class Definition
    {

        public static Definition[] FromJson(string json) => JsonConvert.DeserializeObject<Definition[]>(json, cs481_homework6.Models.Converter.Settings);
        //This function converts the JSON data into the specific C# objects to be used.(Deserialize)
    }
    public static class Serialize
    {
        public static string ToJson(this Definition[] self) => JsonConvert.SerializeObject(self, cs481_homework6.Models.Converter.Settings);
        //This function converts the C# objects back to JSON (Serialize)
    }

    internal static class Converter
    {
        //converter class has a helper for the serialize and deserialize functions.
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
